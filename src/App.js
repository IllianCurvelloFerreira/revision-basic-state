import "./App.css";
import { Component } from "react";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      showh1: true,
      showdiv: true,
      showh2: true,
      showh3: true,
      p: true,
    };
  }

  showButton = () => {
    this.setState({ show: true });
  };

  hideButton = () => {
    this.setState({ show: false });
  };
  showButtonh1 = () => {
    this.setState({ showh1: true });
  };

  hideButtonh1 = () => {
    this.setState({ showh1: false });
  };

  showButtondiv = () => {
    this.setState({ showdiv: true });
  };

  hideButtondiv = () => {
    this.setState({ showdiv: false });
  };

  showButtonh2 = () => {
    this.setState({ showh2: true });
  };

  hideButtonh2 = () => {
    this.setState({ showh2: false });
  };
  ShowAndhideButtonH3 = () => {
    const { showh3 } = this.state;
    this.setState({ showh3: !showh3 });
  };

  ShowAndhideButtonP = () => {
    const { p } = this.state;
    this.setState({ p: !p });
  };

  render() {
    return (
      <div>
        <button onClick={this.hideButton}>Esconda Div!</button>
        <button onClick={this.showButton}>Mostra Div!</button>
        {this.state.show && <p>Olá React!</p>}
        <button onClick={this.hideButtonh1}>Esconda h1!</button>
        <button onClick={this.showButtonh1}>Mostra h1!</button>
        {this.state.showh1 && <h1>"Olá, h1"</h1>}
        <button onClick={this.hideButtondiv}>Esconda Div!</button>
        <button onClick={this.showButtondiv}>Mostra Div!</button>
        {this.state.showdiv && <div>"Olá, div"</div>}
        <button onClick={this.hideButtonh2}>Esconda h2!</button>
        <button onClick={this.showButtonh2}>Mostra h2!</button>
        {this.state.showh2 && <p>"Olá, h2</p>}
        <button onClick={this.ShowAndhideButtonH3}>Esconde/Mostra h3!</button>
        {this.state.showh3 && <p>"Olá, h3</p>}
        <button onClick={this.ShowAndhideButtonP}>Esconde/Mostra p!</button>
        {this.state.p && <p>"Olá, p</p>}
      </div>
    );
  }
}

export default App;
